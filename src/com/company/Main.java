package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    /**
     * This methods takes the file to a text file, that should countain a list of words
     * (one word per line). It extracts the words contained in the file and them
     * in a list of Strings.
     *
     * @param filePath
     * @return
     */
    public static List<String> extractWordsFromFile(String filePath) {
        // Instantiate a new empty list of string
        List<String> listOfWords = new ArrayList<String>();

        // TODO implement


        return listOfWords;
    }


    /**
     * This methods takes a List of words that are already sorted, and a new word
     * that must be inserted in the list of words at the correct place for the list to remain
     * sorted.
     *
     * The first parameter (sortedWords) is modified by the function
     *
     * @param sortedWords The list of already sorted words. This list is modified by the method
     * @param wordToInsert The new word to insert in sortedWords
     */
    public static void insertWordInSortedList(List<String> sortedWords, String wordToInsert) {
        // TODO implement
    }

    public static void printListOfStrings(List<String> strings) {
        System.out.println(String.join(", ", strings));
    }


    public static void sortWordsFromFile() {
        // 1. Extract the words of the text file in a List
        List<String> unsortedWords = extractWordsFromFile("./words.txt");

        // 2. Create an empty list in which we will put the word sorted by alphabetical order
        List<String> sortedWords = new ArrayList<String>();

        // 3. Algorithm : take each word of the unsortedWords list, and insert
        // it at the correct place in the sortedWords list
        for (String wordToSort : unsortedWords) {
            insertWordInSortedList(sortedWords, wordToSort);
        }

        // 4. Display the sorted words
        printListOfStrings(sortedWords);
    }

    public static void main(String[] args) {
        // Si vous implémentez correctement la méthode insertWordInSortedList(),
        // les appels suivants doivent générer le bon affichage

        List<String> sortedWords = new ArrayList<String>();
        sortedWords.add("banana");
        sortedWords.add("zebulon");
        // sortedWords contains ["banana", "zebulon"]
        insertWordInSortedList(sortedWords, "maple");
        // sortedWords should contain ["banana", "maple", "zebulon"]
        System.out.println("First example of sortedWords : ");
        printListOfStrings(sortedWords);


        insertWordInSortedList(sortedWords, "zzz");
        // sortedWords should contain ["banana", "maple", "zebulon", "zzz"]
        System.out.println("Second example of sortedWords : ");
        printListOfStrings(sortedWords);

        insertWordInSortedList(sortedWords, "apple");
        // sortedWords should contain ["apple", "banana", "maple", "zebulon", "zzz"]
        System.out.println("Third example of sortedWords : ");
        printListOfStrings(sortedWords);

        // we replace sortedWords content with an empty List
        sortedWords = new ArrayList<String>();
        // sortedWords contains []
        insertWordInSortedList(sortedWords, "banana");
        // sortedWords should contain ["banana"]
        System.out.println("Fourth example of sortedWords : ");
        printListOfStrings(sortedWords);


        // Maintenant on lance l'algorithme complet sortWordsFromFile() qui trie et affiche
        // les mots contenus dans le fichier words.txt
        sortWordsFromFile();
    }
}
