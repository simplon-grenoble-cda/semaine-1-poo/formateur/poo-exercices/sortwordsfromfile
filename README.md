Objectif :

Lire la liste de mots présents dans le fichier `words.txt`, et afficher sur la console ces mêmes mots
triés par ordre alphabétique.
